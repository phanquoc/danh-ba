import { combineReducers } from 'redux';
import stateApp from './stateApp';
import activeFrom from './activeFrom';

const AppReducer = combineReducers({
    stateApp,
    activeFrom,
});
export default AppReducer;
