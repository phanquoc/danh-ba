import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './container/Home';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import AppReducer from './reducer/index';
import thunk from 'redux-thunk';
import * as serviceWorker from './serviceWorker';

let store = createStore(AppReducer ,applyMiddleware(thunk));
class App extends React.Component {
    render() {
        return (
            <Home />
        )
    }
}

ReactDOM.render(
    <Provider store={store}>

    <App />
</Provider>,
    document.getElementById('root'));

serviceWorker.unregister();
