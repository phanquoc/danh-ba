import React, { Component } from 'react';
import DanhBa from '../components/Danhba';
import ListDanhBa from '../components/ListdanhBa';
import * as Act from './../actions/action';
import { connect } from 'react-redux';
import FormUpdate from '../components/formUpdate';


class Home extends Component {
    getDanhBa = (ho, ten, sdt) => {
        this.props.getDanhBa(ho, ten, sdt);
    }
    update = () => {
        this.props.activeUpdate();
        console.log("quóc");
        
    }
    checkForm = (check) => {
        if(check === true){
            return (
                < DanhBa
                update = {this.update}
                getDanhBa={this.getDanhBa}
            />
            )
        }
        else{
            return (
                < FormUpdate
              
                />
            )
        }
    }
    update = (ho,ten,sdt) => { 

        this.props.activeUpdate();
    }
    render() {
        console.log(this.props.check);
        

        // console.log(this.props.List);

        let element = this.props.List.map((data, i) => {
            return (
                <React.Fragment key={i}>
                    <ListDanhBa
                        item={data}
                        index={i}
                        update = {this.update}
                        // activeUpdate = { this.props.check}
                    />
                </React.Fragment>
            );
        });
        return (
            <div className="row">
               {this.checkForm(this.props.check)}
                <div className="container">
                <h2 className="text-center">Danh sách danh bạ</h2>
                    <table className="table table-condensed">
                        <thead>
                            <tr>
                                <th>Họ </th>
                                <th>Tên</th>
                                <th>SĐT</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            {element}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        List: state.stateApp,
        check: state.activeFrom

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getDanhBa: (ho, ten, sdt) => {
            dispatch(Act.getDanhBa(ho, ten, sdt));
        },
        activeUpdate : () =>{
            dispatch(Act.activeUpdate());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);
